<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"/>
    <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic&display=swap" rel="stylesheet"/>

    <style type="text/css">
        #header-container {
            background-color:rgb(230, 230, 230);
        }

        body {
            font-family: 'Nanum Gothic', sans-serif;
        }

        .link-card {
            border-color:#444444;
        }

        .col-xs-12 {
            background-color:red;
        }

        button {
            white-space:normal;
        }
    </style>
</head>
<body>
    <?php include('navbar.php') ?>

    <div id="header-container">
        <div class="container text-center p-5" >
            <div class="row">
                <p >
                    <h1 class="col-12 text-center">Balance financiero</h1>
                    <div class="col-12 text-center">
                        Acá podes controlar los movimientos de tu cuenta   
                    </div>
                </p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="table-responsive mt-4">
            <table class="table">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Descripción</th>
                        <th>Importe</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>29/04/2019</th>
                        <th>Salario</th>
                        <td>100</td>
                        <td>1000</td>
                    </tr>
                </tbody>
            </table>
     </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
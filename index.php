<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"/>
    <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic&display=swap" rel="stylesheet"/>

    <style type="text/css">
        #header-container {
            background-color:rgb(230, 230, 230);
        }

        body {
            font-family: 'Nanum Gothic', sans-serif;
        }

        .link-card {
            border-color:#444444;
        }

        .col-xs-12 {
            background-color:red;
        }

        button {
            white-space:normal;
        }
    </style>
</head>
<body>
   <?php include('navbar.php') ?>

    <div id="header-container">
        <div class="container text-center p-5" >
            <div class="row">
                <p >
                    <h1 class="col-12 text-center">Bienvenido a MBanking, que queres hacer hoy?</h1>
                    <div class="col-12 text-center">
                        En este sitio podes operar con tu cuenta mirando el balance, haciendo transferencias, pagando servicios y armando inversiones.
                    </div>
                </p>
            </div>
        </div>
    </div>
    <div class="container pt-4">
        <div class="card-deck mb-4">
            <div class="card link-card border rounded p-3 ">
                <div class="text-center">
                    <img class="card-img-top" src="images/balance.png" alt=""/>
                    <div class="card-body">
                        <h5 class="text-center card-title mt-3">Balance</h5>
                        <div class="text-center">Mira como vienen sus cuentas: Ingresos y Egresos.</div>
                    </div>
                    <div class="card-footer border-0 bg-white text-center">
                        <a class="btn btn-primary text-white mt-3" href="balance.php">
                            Ver Situación<br class="d-sm-block d-lg-none"/> Económica
                        </a>
                    </div>
                </div>
            </div>
            <div class="w-100 d-md-none"></div>
            <div class="card link-card text-center border rounded p-3">
                <div class="">
                    <img class="card-img-top" src="images/pagodeservicios.png" alt=""/>
                    <div class="card-body">
                        <h5 class="text-center card-title  mt-3">Pago de Servicios</h5>
                        <div class="text-center">Paga todo lo que necesites desde la comodidad de tu casa,</div>
                    </div>
                    <div class="card-footer border-0 bg-white text-center">
                        <a class="btn btn-primary text-white mt-3">Pagar Servicios</a>
                    </div>
                </div>
            </div>
            <div class="w-100 d-md-none"></div>
            <div class="card link-card text-center border rounded p-3">
                <div class="">
                    <img class="card-img-top" src="images/inversiones.png" alt=""/>
                    <div class="card-body">
                        <h5 class="text-center card-title  mt-3">Inversiones</h5>
                        <div class="text-center">Duplica tus ahorros en el mercado financiero.</div>
                    </div>
                    <div class="card-footer border-0 bg-white text-center">
                        <button class="btn btn-primary text-white mt-3">Invertir en cosas</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>